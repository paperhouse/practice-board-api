package com.practice_board.mapper;

import java.util.HashMap;
import java.util.List;

import com.practice_board.vo.BoardVO;
import com.practice_board.vo.FileVO;

/**
 * API 관리 기능 모델 인터페이스
 * @author KBS
 */
public interface BoardMapper {
	
//	public List<HashMap<String, Object>> selectTestDb(HashMap<String, Object> cond) throws Exception;	
	// 게시글 목록
	public List<BoardVO> boardList(HashMap<String, Object> cond) throws Exception;	
	// 게시글 상세
	public BoardVO boardView(String bo_no) throws Exception;
	// 게시글 작성
	public int boardInsert(BoardVO board) throws Exception;
	// 게시글 수정
	public int boardUpdate(BoardVO board) throws Exception;
	// 게시글 삭제
	public int boardDeleteFlag(String bo_no) throws Exception;
	// 조회수
	public int boardHit(String bo_no) throws Exception;
	// 게시글 수
	public int countArticle(HashMap<String, Object> param) throws Exception;
	// 파일 업로드
	public int fileInsert(FileVO file) throws Exception;
	// 파일 다운로드
	public FileVO fileDetail(String bo_no) throws Exception;
	// 파일 삭제
	public String fileDelete(String bo_no) throws Exception;
	// 파일 수정
	public String fileUpdate(FileVO file) throws Exception;
}
