package com.practice_board.controller;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.io.FilenameUtils;
import org.apache.commons.lang3.RandomStringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.multipart.MultipartHttpServletRequest;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.practice_board.service.BoardService;
import com.practice_board.vo.BoardVO;
import com.practice_board.vo.FileVO;
import com.practice_board.vo.PagingVO;

@RestController
public class BoardController {

	BoardVO board = new BoardVO();

	@Autowired
	BoardService boardService;

	/**
	 * 게시판 전체 리스트
	 * 
	 */
	@RequestMapping("/")
	public Map<String, Object> getBoardList(
			HttpServletRequest request, 
			HttpServletResponse response,
			@RequestParam(value = "startDate", required = false) String startDate,
			@RequestParam(value = "endDate", required = false) String endDate,
			@RequestParam(value = "subject", required = false) String subject,
			@RequestParam(value = "searchType", required = false) String searchType,
			@RequestParam(value = "searchWord", required = false) String searchWord,
			@RequestParam(value = "curPage",required = false) String curPage,
			@RequestParam(value = "sort",required = false) String sort,
			@RequestParam(value = "divine",required = false) String divine
			) throws Exception{

			HashMap<String, Object> map = new HashMap<String, Object>();
	
			Map<String, Object> retunMAp = new HashMap<String, Object>();
			
			System.out.println("startDate>>>>>>>>>>>>>>>>>>>>>>>" + startDate);
			System.out.println("endDate>>>>>>>>>>>>>>>>>>>>>>>" + endDate);
			System.out.println("subject>>>>>>>>>>>>>>>>>>>>>>>" + subject);
			System.out.println("searchType>>>>>>>>>>>>>>>>>>>>>>>" + searchType);
			System.out.println("searchWord>>>>>>>>>>>>>>>>>>>>>>>" + searchWord);
			System.out.println("curPage::" + curPage);
			System.out.println("sort>>>>>>>>>>>>>>>>>>>>>>>" + sort);
			System.out.println("divine>>>>>>>>>>>>>>>>>>>>>>>" + divine);
			System.out.println("++++++chk controller++++++");
			
			map.put("startDate", startDate);
			map.put("endDate", endDate);
			map.put("subject", subject);
			map.put("searchType", searchType);
			map.put("searchWord", searchWord);
			map.put("curPage", curPage);
			map.put("sort", sort);
			map.put("divine", divine);
			
			int count = boardService.countArticle(map);
	
			int cur = 1;
			
			if(null!=curPage && !"".equals(curPage)) {
				cur = Integer.parseInt(curPage);
			}
			
			PagingVO paging = new PagingVO(count, cur, divine);
			int start = paging.getPageBegin();
			int end = paging.getPageEnd();

			map.put("start", start);
			map.put("end", end);
			map.put("count", count); // 레코드 개수
			
			List<BoardVO> list = boardService.boardList(map);
			
			retunMAp.put("list", list);
			retunMAp.put("paging", paging);
			retunMAp.put("count", count);
			
			return retunMAp;
	}

	/**
	 * 게시판 상세보기
	 */
	@RequestMapping("/view")
	public Map<String, Object> getBoard(
			HttpServletRequest request,
			@RequestParam(value = "bo_no", required = false) String bo_no,
			@RequestParam(value = "chkDivision", required = false) String chkDivision
			) throws Exception {
		
			System.out.println("view controller  chkDivision=====" + chkDivision);

			Map<String, Object> map = new HashMap<String, Object>();
	
			BoardVO board = boardService.boardView(bo_no);
			FileVO files = boardService.fileDetailService(bo_no);
			
			board.setBo_no(request.getParameter("bo_no"));
			
			if("V".equals(chkDivision)) {
				boardService.boardHit(bo_no);
			}
	
			System.out.println("view controller  bo_no=====" + bo_no);
	
			map.put("board", board);
			map.put("files", files);
			
			return map;

	}

	/**
	 * 게시판 글쓰기(insert)
	 * 
	 */
	
	@RequestMapping("/insert.do")
	public Map<String, Object> getBoardInsert(
			HttpServletRequest request) throws Exception {
			
			Map<String, Object> map = new HashMap<String, Object>();
			FileVO file = new FileVO();
			
			MultipartHttpServletRequest multipartRequest = (MultipartHttpServletRequest)request;
			
			MultipartFile files = multipartRequest.getFile("files");
			System.out.println("FILE>>>>>>>>>>>>>>>>>"+multipartRequest.getParameter("files"));
			System.out.println("FILE>>>>>>>>>>>>>>>>>"+files);
			
			board.setBo_title(request.getParameter("bo_title"));
			board.setBo_writer(request.getParameter("bo_writer"));
			board.setBo_edit_writer(request.getParameter("bo_edit_writer"));
			board.setBo_sub(request.getParameter("bo_sub"));
			board.setBo_content(request.getParameter("bo_content"));
			
//////////////////////////////////파일 업로드//////////////////////////////////////////////////////////		
			if(files== null) {
				boardService.boardInsert(board);
			}else {
				String fileName = files.getOriginalFilename();
				String fileNameExtension = FilenameUtils.getExtension(fileName).toLowerCase();
				File destinationFile;
				String destinationFileName;
				String file_Url = "C:/board/api/practice_board/src/main/webapp/uploadFiles/";
						
				do {
					destinationFileName = RandomStringUtils.randomAlphanumeric(32)+"."+fileNameExtension;
					System.out.println("DESTINATIONFILENAME>>>>>>>>>>>>>>>>>"+ destinationFileName);
					destinationFile = new File(file_Url + destinationFileName);
				}while(destinationFile.exists());
				
				destinationFile.getParentFile().mkdirs();
				System.out.println("DESTINATIONFILE>>>>>>>>>>>>>>>>>"+ destinationFile);
				files.transferTo(destinationFile);
				
				int insertBoard = boardService.boardInsert(board);
				
				map.put("insertBoard", insertBoard);

				file.setBo_no(board.getBo_no());
				file.setFileName(destinationFileName);
				file.setFileOriName(fileName);
				file.setFile_Url(file_Url);
				
				boardService.fileInsertService(file); // file insert
			}
///////////////////////////////////////////////////////////////////////////////////////////////////		
			
			return map;
				
	}


	/**
	 * 게시판 수정 update.do
	 */
	@RequestMapping("/update.do")
	public Map<String, Object> setBoardUpdate(
			HttpServletRequest request,
			@RequestParam(value = "bo_no", required = false) String bo_no) throws Exception {
			
			FileVO fileVO = new FileVO();
			board.setBo_no(request.getParameter("bo_no"));
			board.setBo_title(request.getParameter("bo_title"));
			board.setBo_sub(request.getParameter("bo_sub"));
			board.setBo_edit_writer(request.getParameter("bo_edit_writer"));
			board.setBo_content(request.getParameter("bo_content"));
			fileVO.setFileOriName(request.getParameter("fileOriName"));
			
			System.out.println("request.getParameter(\"fileOriName\")>>>>>" + request.getParameter("fileOriName"));
			Map<String, Object> map = new HashMap<String, Object>();
	
			int updateDoBoard = boardService.boardUpdate(board);
//			String updateFile = boardService.fileUpdate(fileVO);
			int fileUpdate = boardService.fileInsertService(fileVO); 
			
			map.put("updateDoBoard", updateDoBoard);
			map.put("fileUpdate", fileUpdate);
			
			System.out.println("updateDoBoard:" + updateDoBoard);
			System.out.println("fileUpdate>>" + fileUpdate);
			
			return map;

	}

	/**
	 * 게시판 글삭제(delete)
	 */
	@RequestMapping("/delete.do")
	public Map<String, Object> getBoardDelete(
			HttpServletRequest request,
			@RequestParam(value = "bo_no", required = false) String bo_no
			) throws Exception {

			Map<String, Object> map = new HashMap<String, Object>();
	
			int board = boardService.boardDeleteFlag(bo_no);
	
			map.put("board", board);
	
			return map;

	}
	
	/**
	 * 파일 다운로드(file DownLoad)
	 * */
	@RequestMapping("/fileDown.do")
	public String fileDown(
//			@PathVariable String bo_no,
			@RequestParam(value = "bo_no", required = false) String bo_no,
			HttpServletRequest request,
			HttpServletResponse response) throws Exception  {
			
			request.setCharacterEncoding("UTF-8");
			FileVO fileVO = boardService.fileDetailService(bo_no);
			
			System.out.println("FILEDOWN:BO_NO:" + bo_no);
			try {
				// 파일 업로드된 경로
				String fileUrl = fileVO.getFile_Url();
				//fileUrl +="/";
				String savePath = fileUrl;
				String fileName = fileVO.getFileName();
				
				// 실제 내보낼 파일명
				String oriFileName = fileVO.getFileOriName();
				InputStream in = null;
				OutputStream os = null;
				File file = null;
				boolean skip = false;
				String client = "";
				
				// 파일을 읽어 스트림에 담기
				System.out.println("+++++fileName+++++"+fileName);
				System.out.println("+++++savePath+++++"+savePath);
				
				try {
					file = new File(savePath,fileName);
					in = new FileInputStream(file);
				}catch(FileNotFoundException fe) {
					skip = true;
				}
				
				client = request.getHeader("User-Agent");
				
				// 파일 다운로드 헤더 지정
				response.reset();
				response.setContentType("application/octet-stream");
				response.setHeader("Content-Description", "JSP Generated Data");
				
				if(!skip) {
					System.out.println("<><><><><><>><>>");
					 // IE
	                if (client.indexOf("MSIE") != -1) {
	                    response.setHeader("Content-Disposition", "attachment; filename=\""
	                            + java.net.URLEncoder.encode(oriFileName, "UTF-8").replaceAll("\\+", "\\ ") + "\"");
	                    System.out.println("1");
	                    // IE 11 이상.
	                } else if (client.indexOf("Trident") != -1) {
	                    response.setHeader("Content-Disposition", "attachment; filename=\""
	                            + java.net.URLEncoder.encode(oriFileName, "UTF-8").replaceAll("\\+", "\\ ") + "\"");
	                    System.out.println("2");
	                } else {
	                    // 한글 파일명 처리
	                    response.setHeader("Content-Disposition",
	                            "attachment; filename=\"" + new String(oriFileName.getBytes("UTF-8"), "ISO8859_1") + "\"");
	                    response.setHeader("Content-Type", "application/octet-stream; charset=utf-8");
	                    System.out.println("3");
	                }
	
	                response.setHeader("Content-Length", "" + file.length());
	                os = response.getOutputStream();
	                
	                byte b[] = new byte[(int)file.length()];
	                int leng = 0;
	                while((leng = in.read(b))>0) {
	                	os.write(b, 0 , leng);
	                	os.flush();
	                }
				}else {
					response.setContentType("text/html;charset=UTF-8");
		            System.out.println("<script language='javascript'>alert('파일을 찾을 수 없습니다');history.back();</script>");
				}
				in.close();
				os.close();
			}catch(Exception e) {
				System.out.println("ERROR:"+ e.getMessage());
			}
			
			return null;
	}
	
	/**
	 * 파일 삭제(delete)
	 */
	@RequestMapping("/fileDelete.do")
	public Map<String, Object> fileDelete(
			HttpServletRequest request,
			@RequestParam(value = "bo_no", required = false) String bo_no
			) throws Exception {

			Map<String, Object> map = new HashMap<String, Object>();
	
			String board = boardService.fileDelete(bo_no);
			
			map.put("board", board);
			
			return map;

	}

}
