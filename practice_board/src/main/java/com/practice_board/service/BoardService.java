package com.practice_board.service;

import java.util.HashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.practice_board.mapper.BoardMapper;
import com.practice_board.vo.BoardVO;
import com.practice_board.vo.FileVO;

/**
 * API 관리 기능 서비스 클래스
 * @author KBS
 */
@Service
public class BoardService {

	@Autowired
	BoardMapper boardMapper;
	
//	public List<HashMap<String, Object>> selectTestDb(HashMap<String, Object> param) throws Exception {
//	return boardMapper.selectTestDb(param);
//	}
	// 게시글 목록
	public List<BoardVO> boardList(HashMap<String, Object> param) throws Exception {
		return boardMapper.boardList(param);
	}
	// 게시글 상세
	public BoardVO boardView(String bo_no) throws Exception {
		return boardMapper.boardView(bo_no);
	}
	// 게시글 작성
	public int boardInsert(BoardVO board) throws Exception{
		return boardMapper.boardInsert(board);
	}
	// 게시글 수정
	public int boardUpdate(BoardVO board) throws Exception{
		return boardMapper.boardUpdate(board);
		
	}
	// 게시글 삭제
	public int boardDeleteFlag(String bo_no) throws Exception{
		return boardMapper.boardDeleteFlag(bo_no);
	}
	// 조회수
	public int boardHit(String bo_no) throws Exception{
		return boardMapper.boardHit(bo_no);
	}
//	 레코드 갯수
	public int countArticle(HashMap<String, Object> param) throws Exception{
		return boardMapper.countArticle(param);
	}
	// 파일 업로드
	public int fileInsertService(FileVO file) throws Exception{
	    return boardMapper.fileInsert(file);
	}
	// 파일 다운로드
	public FileVO fileDetailService(String bo_no) throws Exception{
		return boardMapper.fileDetail(bo_no);
	}
	// 파일 삭제
	public String fileDelete(String bo_no) throws Exception{
		return boardMapper.fileDelete(bo_no);
	}
	// 파일 수정
	public String fileUpdate(FileVO file) throws Exception{
		return boardMapper.fileUpdate(file);
	}
}
