package com.practice_board.config;

import java.io.IOException;
import java.net.URI;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

/**
 * Insert CORS Filter setting into web.xml
 *
  <filter>
    <filter-name>cors</filter-name>
    <filter-class>kr.co.kbs.api.CorsFilter</filter-class>
    <async-supported>true</async-supported>
  </filter>
  <filter-mapping>
    <filter-name>cors</filter-name>
    <url-pattern>/api/v1/*</url-pattern>
  </filter-mapping>
 *
 * @author hslee
 *
 */
@Component
public class CorsFilter implements Filter {
	private final Logger logger = LoggerFactory.getLogger(CorsFilter.class);

	private final static String DEFAULT_ALLOW_ORIGIN = "http://dwww.kbs.co.kr";
	//	private final static String DEFAULT_ALLOW_ORIGIN = "http://dx5jyx5ckom82.cloudfront.net";
	private final static String DEFAULT_ALLOW_HEADER = "Origin,Accept,X-Requested-With,Content-Type,Access-Control-Request-Method,Access-Control-Request-Headers,Authorization";

	private final static String[] PREVIEW_ALLOW_URI = {"dpluspreview.kbs.co.kr", "pluspreview.kbs.co.kr"};

	@Override
	public void doFilter(ServletRequest req, ServletResponse res,
		FilterChain chain) throws IOException, ServletException {
		String allowOrigin = DEFAULT_ALLOW_ORIGIN;
		String allowHeader = DEFAULT_ALLOW_HEADER;

		HttpServletRequest request = (HttpServletRequest)req;
		HttpServletResponse response = (HttpServletResponse)res;

		String origin = request.getHeader("origin");
		String referer = request.getHeader("referer");

		System.out.println("origin >>>> " + origin);
		System.out.println("referer >>>> " + referer);

		// ---------------------------------
		// CORS filter
		if (!StringUtils.isEmpty(origin)) {
			allowOrigin = origin;
		} else if (!StringUtils.isEmpty(referer)) {
			try {
				URI uri = new URI(referer);
				allowOrigin = uri.getScheme() + "://" + uri.getHost();
				if (uri.getPort() != -1 && (uri.getPort() != 80 || uri.getPort() != 443)) {
					allowOrigin += ":" + uri.getPort();
				}
			} catch (Exception e) {
				logger.warn(e.getMessage(), e);
			}
		} else {
			allowOrigin = "*";
		}

		// ---------------------------------
		// preview filter
		if (!StringUtils.isEmpty(referer)) {
			try {
				URI uri = new URI(referer);
				String refererParams = uri.getQuery();

				if (refererParams != null) {
					String[] refererParamArray = refererParams.split("&");

					for (String param : refererParamArray) {
						if (param.substring(0, param.indexOf('=')).equals("v")) {
							if (param.substring(param.indexOf('=') + 1).equals("p")) {
								request.setAttribute("preview_mode", true);
							}
						}
					}
				}
			} catch (Exception e) {
				logger.warn(e.getMessage(), e);
			}
		}

		System.out.println("allowOrigin >>>>> " + allowOrigin);

		response.setHeader("Access-Control-Allow-Methods", "*"); // "POST, GET, OPTIONS, PUT, DELETE"
		response.setHeader("Access-Control-Max-Age", "3600");
		response.setHeader("Access-Control-Allow-Headers", allowHeader);
		response.setHeader("Access-Control-Allow-Origin", allowOrigin);
		response.setHeader("Access-Control-Allow-Credentials", "true"); // true, to use cookie & http auth

		// Just ACCEPT and REPLY OK if OPTIONS
		/*
		if ( request.getMethod().toString().equals("OPTIONS") ) {
		    response.setStatus(HttpServletResponse.SC_OK);
		    return;
		}
		*/
		chain.doFilter(req, res);
	}

	@Override
	public void init(FilterConfig filterConfig) {}

	@Override
	public void destroy() {}
}
