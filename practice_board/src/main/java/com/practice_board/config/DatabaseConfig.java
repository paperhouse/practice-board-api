package com.practice_board.config;

import javax.sql.DataSource;

import org.apache.ibatis.session.SqlSessionFactory;
import org.mybatis.spring.SqlSessionFactoryBean;
import org.mybatis.spring.SqlSessionTemplate;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.context.ApplicationContext;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.support.PathMatchingResourcePatternResolver;

/**
 * Database Configuration 클래스
 * @author KBS
 */
@Configuration
@MapperScan({"com.practice_board.mapper"})
public class DatabaseConfig {
	
	@Bean
	public SqlSessionFactory sqlSessionFactory(DataSource dataSource, ApplicationContext applicationContext ) throws Exception{
		final SqlSessionFactoryBean sessionFactory = new SqlSessionFactoryBean();
		sessionFactory.setDataSource(dataSource);
		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
		sessionFactory.setMapperLocations(resolver.getResources("classpath:mapper/*.xml"));
		return sessionFactory.getObject();
	}
	

	/*
	 * Usage :
	 * <pre>
	 * 		RoutingContext.setRouterId("bbs1");
	 * 		try { ... } catch(Exception e) { ... }
	 * 		RoutingContext.clearRouterId();;
	 * </pre>
	 * @param dataSource
	 * @param applicationContext
	 * @return
	 * @throws Exception
	 */
//	@Bean
//	public SqlSessionFactoryBean sqlSessionFactoryBean(DataSource dataSource, ApplicationContext applicationContext)
//		throws Exception {
//
//		RoutingDataSource routingDataSource = new RoutingDataSource(dataSource, env);
//		final SqlSessionFactoryBean sessionFactoryBean = new SqlSessionFactoryBean();
//		// For multiple database routing : ignore default datasource map to RoutingDataSource
//		sessionFactoryBean.setDataSource(routingDataSource);
//
//		PathMatchingResourcePatternResolver resolver = new PathMatchingResourcePatternResolver();
//		sessionFactoryBean.setMapperLocations(resolver.getResources("classpath:mapper/*.xml"));
//		sessionFactoryBean.setConfigLocation(resolver.getResource("classpath:mybatis/mybatis-config.xml"));
//		return sessionFactoryBean;
//	}

	@Bean
	public SqlSessionTemplate sqlSessionTemplate(SqlSessionFactory sqlSessionFactory) throws Exception {
		final SqlSessionTemplate sqlSessionTemplate = new SqlSessionTemplate(sqlSessionFactory);

		return sqlSessionTemplate;
	}

}
