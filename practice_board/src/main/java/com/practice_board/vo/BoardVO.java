package com.practice_board.vo;

public class BoardVO {
	private String bo_no;
	private String bo_sub;
	private String bo_title;
	private String bo_writer;
	private String bo_edit_writer;
	private String bo_date;
	private String bo_edit_date;
	private int bo_hit;
	private String bo_content;
	private String del_fg;
	private String bo_sort;
	private String code;
	
	public String getBo_no() {
		return bo_no;
	}

	public void setBo_no(String bo_no) {
		this.bo_no = bo_no;
	}

	
	public String getBo_sub() {
		return bo_sub;
	}

	public void setBo_sub(String bo_sub) {
		this.bo_sub = bo_sub;
	}

	public String getBo_title() {
		return bo_title;
	}

	public void setBo_title(String bo_title) {
		this.bo_title = bo_title;
	}

	public String getBo_writer() {
		return bo_writer;
	}

	public void setBo_writer(String bo_writer) {
		this.bo_writer = bo_writer;
	}

	public String getBo_date() {
		return bo_date;
	}

	public void setBo_date(String bo_date) {
		this.bo_date = bo_date;
	}

	public int getBo_hit() {
		return bo_hit;
	}

	public void setBo_hit(int bo_hit) {
		this.bo_hit = bo_hit;
	}

	public String getBo_content() {
		return bo_content;
	}

	public void setBo_content(String bo_content) {
		this.bo_content = bo_content;
	}

	public String getDel_fg() {
		return del_fg;
	}

	public void setDel_fg(String del_fg) {
		this.del_fg = del_fg;
	}

	public String getBo_edit_writer() {
		return bo_edit_writer;
	}

	public void setBo_edit_writer(String bo_edit_writer) {
		this.bo_edit_writer = bo_edit_writer;
	}

	public String getBo_edit_date() {
		return bo_edit_date;
	}

	public void setBo_edit_date(String bo_edit_date) {
		this.bo_edit_date = bo_edit_date;
	}

	public String getBo_sort() {
		return bo_sort;
	}

	public void setBo_sort(String bo_sort) {
		this.bo_sort = bo_sort;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
	

}
