package com.practice_board.vo;

public class FileVO {
	private int file_no;
	private String bo_no;
	private String fileName;
	private String fileOriName;
	private String file_Url;
	
	public int getFile_no() {
		return file_no;
	}
	public void setFile_no(int file_no) {
		this.file_no = file_no;
	}
	public String getBo_no() {
		return bo_no;
	}
	public void setBo_no(String bo_no) {
		this.bo_no = bo_no;
	}
	public String getFileName() {
		return fileName;
	}
	public void setFileName(String fileName) {
		this.fileName = fileName;
	}
	public String getFileOriName() {
		return fileOriName;
	}
	public void setFileOriName(String fileOriName) {
		this.fileOriName = fileOriName;
	}
	public String getFile_Url() {
		return file_Url;
	}
	public void setFile_Url(String file_Url) {
		this.file_Url = file_Url;
	}
}
